import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'

    POSTGRES_USER = 'dms'
    POSTGRES_PW = '123123'
    POSTGRES_URL = 'localhost'
    POSTGRES_DB = 'flask'

    DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}' \
        .format(user=POSTGRES_USER, pw=POSTGRES_PW, url=POSTGRES_URL, db=POSTGRES_DB)

    SQLALCHEMY_DATABASE_URI = DB_URL
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # silence the deprecation warning
