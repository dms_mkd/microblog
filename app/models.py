from app import db


class City(db.Model):
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    addresses = db.relationship('Address', backref='city', lazy=True)

    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_city_name'),
    )

    def __repr__(self):
        return self.name


class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    street = db.Column(db.String(128), nullable=False)
    street_no = db.Column(db.String(16))
    zip_code = db.Column(db.String(5), nullable=False)
    city_id = db.Column(db.Integer, db.ForeignKey('city.id'), nullable=False)

    model_name = db.Column(db.String(32), nullable=False)
    rec_id = db.Column(db.Integer, nullable=False)

    contact = db.relationship('Contact', backref='address', lazy=True)

    def __repr__(self):
        return '{} {}, {} {}'.format(self.street, self.street_no, self.zip_code, self.city)

class Contact(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, nullable=False)

    ref = db.Column(db.String(32))

    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False)

    customer = db.Column(db.Boolean)
    supplier = db.Column(db.Boolean)
    company = db.Column(db.Boolean)

    def __repr__(self):
        return '<Contact {}>'.format(self.name)


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, nullable=False)
    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_company_name'),
    )


class Building(db.Model):
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False)
    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_bld_name'),
    )


class Unit(db.Model):
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=False)
    address_id = db.Column(db.Integer, db.ForeignKey('address.id'), nullable=False)
    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_unit_name'),
    )


class Product(db.Model):
    id = db.Column(db.Integer, nullable=False, primary_key=True)
    name = db.Column(db.String(128), nullable=False)
    code = db.Column(db.String(8), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_product_name'),
    )


class RentContract(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), nullable=False)

    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'), nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'), nullable=False)
    owner_id = db.Column(db.Integer, db.ForeignKey('contact.id'), nullable=False)
    building_id = db.Column(db.Integer, db.ForeignKey('building.id'), nullable=False)
    unit_id = db.Column(db.Integer, db.ForeignKey('unit.id'), nullable=False)

    start_date = db.Column(db.Date, nullable=False)
    end_date = db.Column(db.Date)

    status = db.Column(db.String(16), nullable=False)

    tenants = db.Column(db.String(128))

    commercial = db.Column(db.Boolean)

    __table_args__ = (
        db.UniqueConstraint('name', name='uniq_contract_name'),
    )


class RentContractTotals(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    contract_id = db.Column(db.Integer, db.ForeignKey('rent_contract.id'), nullable=False)

    rent = db.Column(db.Float, nullable=False)
    prepayment = db.Column(db.Float, nullable=False)
    vat = db.Column(db.Float, nullable=False)
    total = db.Column(db.Float, nullable=False)

    __table_args__ = (
        db.UniqueConstraint('contract_id', name='uniq_contract_totals_id'),
    )


class RentLine(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    contract_id = db.Column(db.Integer, db.ForeignKey('rent_contract.id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'), nullable=False)
    amount = db.Column(db.Float, nullable=False)
    line_type = db.Column(db.String(8), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('contract_id', 'product_id', name='uniq_line'),
    )
