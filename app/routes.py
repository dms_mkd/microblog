from app import app
from flask import render_template
from app.forms import NewContactForm, EditContactForm
from app import db
from app.models import Contact, City, Address
from flask import render_template, flash, redirect, url_for
from flask import Flask, Response, render_template, request
from flask import jsonify
import json


@app.route('/')
@app.route('/index')
def index():
    contacts = Contact.query.filter().order_by(Contact.id.desc()).limit(25)
    return render_template('index.html', title='Home', contacts=contacts)


@app.route('/contact', methods=['GET', 'POST'])
def contact():
    form = NewContactForm()
    if form.validate_on_submit():
        address = Address(
            street=form.street.data,
            street_no=form.street_no.data,
            city_id=form.city_id.data,
            zip_code=form.zip_code.data,
            model_name='contact',
            rec_id=0
        )
        db.session.add(address)

        db.session.commit()
        contact = Contact(
            name=form.name.data,
            ref=form.ref.data,
            address_id=address.id,
            customer=form.customer.data,
            supplier=form.supplier.data,
            company=form.company.data
        )
        db.session.add(contact)

        db.session.commit()

        flash('Congratulations, created new contact: {}, on address: {}!'.format(contact, address))
        return redirect(url_for('index'))
    return render_template('new_contact_form.html', title='New Contact', form=form)


@app.route('/autocomplete_city', methods=['GET'])
def autocomplete_city():
    search = request.args.get('q')
    # import pdb; pdb.set_trace()
    print(search)
    cities = City.query.filter(City.name.ilike('%{}%'.format(search))).all()
    # print([city.name for city in cities])
    # cities = ['Berlin', 'Hamburg']
    return jsonify(cities=[{'value': city.name, 'id': city.id} for city in cities])


@app.route('/contact/<int:id>', methods=['GET', 'POST'])
def contact_edit(id):
    qry = db.session.query(Contact).filter(Contact.id == id)
    contact = qry.first()

    form = EditContactForm()

    if not contact:
        flash('Requested page do not exist!')
        return redirect(url_for('index'))

    if form.validate_on_submit() and request.method == 'POST':
        contact.name = form.name.data
        contact.ref = form.ref.data
        contact.customer = form.customer.data
        contact.supplier = form.supplier.data
        contact.company = form.company.data
        db.session.add(contact)
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(('/contact/{}'.format(id)))

    elif request.method == 'GET':
        form.id.data = contact.id
        form.name.data = contact.name
        form.ref.data = contact.ref
        form.street.data = contact.address.street
        form.street_no.data = contact.address.street_no
        form.zip_code.data = contact.address.zip_code
        form.city.data = contact.address.city.name
        form.city_id.data = contact.address.city.id
        form.address_id.data = contact.address_id
        form.customer.data = contact.customer
        form.supplier.data = contact.supplier
        form.company.data = contact.company

    return render_template('edit_contact_form.html', form=form)
