from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, HiddenField, IntegerField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from wtforms.validators import DataRequired
from app.models import Contact


class NewContactForm(FlaskForm):

    name = StringField('Name', validators=[DataRequired()])
    ref = StringField('Reference', validators=[DataRequired()])

    customer = BooleanField('Customer')
    supplier = BooleanField('Supplier')
    company = BooleanField('Company')

    street = StringField('Street', validators=[DataRequired()])
    street_no = StringField('Street No.')
    zip_code = StringField('Zip Code', validators=[DataRequired()])

    city = StringField('City', id='city', validators=[DataRequired()])
    city_id = HiddenField('City ID', id='city_id', validators=[DataRequired()])

    submit = SubmitField('Submit')

    def validate_zip_code(self, zip_code):
        if len(zip_code.data) != 5:
            raise ValidationError('Zip Code must be 5 digits.')

    def validate_ref(self, ref):
        """Method is called automatically because it matches format: validate_{field_name}."""
        ref = ref.data

        if not ref:
            return

        contact = Contact.query.filter_by(ref=ref).first()
        if contact is not None:
            raise ValidationError('Please use different Reference.')


class EditContactForm(FlaskForm):
    id = HiddenField('ID', validators=[DataRequired()])
    name = StringField('Name', validators=[DataRequired()])
    ref = StringField('Reference', validators=[DataRequired()])

    customer = BooleanField('Customer')
    supplier = BooleanField('Supplier')
    company = BooleanField('Company')

    street = StringField('Street', validators=[DataRequired()])
    street_no = StringField('Street No.')
    zip_code = StringField('Zip Code', validators=[DataRequired()])

    city = StringField('City', id='city', validators=[DataRequired()])
    city_id = HiddenField('City ID', id='city_id', validators=[DataRequired()])

    address_id = HiddenField('Address ID', id='address_id', validators=[DataRequired()])

    submit = SubmitField('Submit')

    def validate_zip_code(self, zip_code):
        if len(zip_code.data) != 5:
            raise ValidationError('Zip Code must be 5 digits.')
