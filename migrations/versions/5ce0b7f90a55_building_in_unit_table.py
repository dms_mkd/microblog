"""building in unit table

Revision ID: 5ce0b7f90a55
Revises: d6af6f329672
Create Date: 2019-07-22 22:31:18.296356

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5ce0b7f90a55'
down_revision = 'd6af6f329672'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('unit', sa.Column('building_id', sa.Integer(), nullable=False))
    op.create_foreign_key(None, 'unit', 'building', ['building_id'], ['id'])
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'unit', type_='foreignkey')
    op.drop_column('unit', 'building_id')
    # ### end Alembic commands ###
